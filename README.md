# iFarm v1.00 [prototype]

App is available (for january 2022) to be used at a  Digital Ocean cloud server : [IFarm WEB App](http://143.198.242.29:3000/)  

![iFarm](https://gitlab.com/martin.aedma/farm_backend/-/raw/main/images/home.jpg "iFarm")

## Contents

1. [Overview](#overview)
2. [Tech Stack](#techstack)
3. [App Guide](#appguide)
4. [Demo](#demo)
5. [Conclusion](#conclusion)

## Overview

This application was created as my solution to [Solita Dev Academy PreTask - Spring 2022](https://github.com/solita/dev-academy-2022-exercise).
Application idea is to store data from farms and then display them to users. Data means metric data - such as temperatures, pH, rainFall. Data is uploaded to app via  .csv files. Users can then view metric data and get analyzed results. This web app is full-stack version, featuring a backend system with database for storing this data and a frontend UI for displaying this data in different ways. 

## Tech Stack

There were no limits to which technologies could be used, developers choise. I have familiarity with several programming languages and technologies, however, since I have worked as a frontend developer with React / JavaScript I decided to go full stack with JavaScript. That means backend was developed with NodeJS(Express). In a word - MERN stack

![MERN stack](https://swonigatechnology.com/wp-content/uploads/2021/03/Blog-Article-MERN-Stack.jpg "MERN stack")

### Database : MongoDB

In several of my school projects I have used SQL databases (MySQL and SQLite) and in only one NoSQL (MongoDB). I really liked the "freedom" of MongoDB of not having to be too rigid (everything defined) with every table and it's contents. MongoDB gave me a much better developer experience where I could start off quickly. You still have the option to be very rigid if you choose to (defining relations and data types), but its not mandatory. Curly bracketed, Javascript Object like syntax also seemes to go very smoothly with React / NodeJS stack as all syntax looks very similar.

### Backend : NodeJS

I have much less experience developing backend API's than doing frontend. However, it's something that I definitely want to do and be a full stack developer. Prior to this application I have made only one REST API with CRUD functionality before. That was with PHP (Laravel). I have looked at NodeJS as a first proper backend skill to have. I found it very pleasent, quite simple and straightforward to get working and start developing with Express. It took very few lines of code to get server running and routes up. In the long run I do want to broadedn my skills with other technologies like C#/.NET which seems to be also in high demand.

### Frontend : React (NextJs)

I have used React before and have made several UI projects with it. I quite enjoyed this technology ever since I first used it. This was really my only choice to use right now, also React seems to very in demand and popular for frontend development. I also chose NextJs instead of Create-React-App as to get more experience with NextJs and Server Side Rendering.

### Cloud Deployment : Digital Ocean Droplet

I've used DO virtual machines before (droplets) and I chose to deploy this app for simple use / testing to cloud server. I tinkered a bit with Microsoft Azure as well but I ran into some issues which I couldnt resolve immediately and as time was running out I did a quick deployment with Digital Ocean. I meant to use Docker containers, as I have done it once before in school, but had no time left.

## App Guide

### Map : Home Screen

On Home screen there is a low poly 3D model of Earth spinning (developed with React Three Fiber- ThreeJs) which has locations of each Farm in database mapped with a wireframe looking Box. Hovering over a farm will show you name and "location", box will also start to spin. When new data is uploaded to database, Map will detect new farms and add them. But this system is not finished.

### Menu : Navigation

On right side of screen there is a menu of 5 icons which are used to navigate the App. Globe will return to Home screen. Table and Chart icons are for either viewing query results in table or chart format. Search icon directs use to Query builder where data search can be built and executed. Finally the plus icon takes user to file upload where new data can be uploaded to database.

### Query Builder : Search Data

![QueryBuilder](https://gitlab.com/martin.aedma/farm_backend/-/raw/main/images/query.png "Query Builder")

Here user can build a desired query by using several options. This is how querybuilder works:

* Location - by pressing dropdown arrow a list with all farm names in databse will show. Text are can be used to filter this list. When clicking on farm name it will be added as a parameter for query. When left empty (no locations chosen) by default all locations will be chosen.

* Metric - which metrics are included in search. When none is chosen, default is to include all of them

* Granularity - Day / Month / Year. I probably misunderstood what granularity means where, but when one of those is chosen data from this option will be searched for. For example, if day is chosen all data from each day will be shown between specififed time period. If no time period is specified by user, current date is chosen by default. If one date is chosen, data from that date will be searched for. And if both dates are chosen, data from this time period will be searched for. With Month or Year checked, app will extract months (or years) which are in specified time period, or if no time is set by user, current month / year will be chosen by default. Another example is that if Year is chosen and no date specified, current year will be chosen by default and query searches for data from 1.1.2021 - 31.12.2021.
When serching by granularity, it is best to search one specific metric at a time (pH or rainFall or temperature) because otherwise it will be hard to read data from chart.
Important to know is that when granularity option is checked AND user also chooses either Average or MinMax, then granularity will have no effect. MinMax and Average overwrite granularity.

* Statistsical Queries : MinMax - will look for minimal and maximal values of target metrics between specified time period, or if no period is chosen current date is chosen as default.

*  Statistsical Queries : Average - will take monthly average values of target metrics between specified time period, or if no period is chosen current date is chosen as default.

When building queries it is best to specify a time period (start / finish) from which data will be searched for.

### Results 1/2 : Chart View

![Chart View](https://gitlab.com/martin.aedma/farm_backend/-/raw/main/images/chart.png "Chart View")

Chart view is default result view to be presented when query is executed. It will contain query results in chart. Example query is for all locations in database, all metrics with monthly Average option. Dates are specified as 1.1.2019 and 1.1.2020. So for each month between those dates, average value of all metrics is shown. When more than one location is included in search, chart icon in Menu is replaced with Arrow icon which allows to cycle charts for each location. In the example picture, on lower right corner of chart there is indication of "1/2 Friman Metsola Collective" which means that there were two locations in query and you can cycle them.

### Results 2/2 : Table View

![Table View](https://gitlab.com/martin.aedma/farm_backend/-/raw/main/images/table.png "Table View")

Every query result can be also viewed in Table format. This is data from which charts are built. You can switch between chart or table view by clicking theyre corresponding icons in the menu.

### Add Farm Data : Upload file(s)

Finally, you can also upload new Data to database by clicking + icon in the menu. Up to 4 files can be selected per upload and files have to be in specific 
.csv format and structure for upload to begin & succeed.

## Demo

Watch on [YOUTUBE](https://youtu.be/O-9Uj5UGeFw)

## Conclusion

This was an excellent task to practice full stack development and I learned a lot by doing this!
There were many features which I did not manage to implement as I only had 3 weeks to work on this with limited time each day. In its current state App is a prototype, able to display and get a feeling for what final product is like. There are many bugs that I know of and possibly many more that I'm not even aware of :) For example I managed to do only very little error handling, which quite possibly can lead to some unexpected crashes. No testing was implemented and so on. Either way I am happy with what I managed to do in a relatively short time period.
