export const validateLocationData = (a, b, c, name) => {
    const x = Number(a)
    const y = Number(b)
    const z = Number(c)

    if(isNaN(x) || isNaN(y) || isNaN(z) || typeof name === 'undefined'){
        return false
    }

    return true
}


export const validateDataRow = ({sensorType, value}) => {

    const type = sensorType.toLowerCase()
    const number = Number(value)

    if(isNaN(number)){
        return false
    }

    switch(type){
        case 'rainfall':
            if (number < 0 || number > 500){
                return false
            }
            break;
        case 'ph':
            if(number < 0 || number > 14){
                return false
            }
            break;
        case 'temperature':
            if(number < -50 || number > 100){
                return false
            }
            break;
        default:
            return false
    }

    return true
}

export const createMatch = (location, sensorType, datetime ) => {

    let match = {}

    if ( typeof location === 'string'){
        match = {...match, location: location}
    }

    if (Array.isArray(location)){
        match = {...match, location: { $in: location} }
    }

    if (typeof sensorType === 'string'){
        match = {...match, sensorType: sensorType}
    }

    if (Array.isArray(sensorType)){
        match = {...match, sensorType: { $in: sensorType}}
    }

    // TODO: Make date formatting in frontend
    if (typeof datetime === 'string') {
        const start = new Date(datetime)
        const end = new Date(start.getTime()+86400000)
        match = {...match, datetime: { $gte: start, $lt: end }}
    }

    if (Array.isArray(datetime)){
        const start = new Date(datetime[0])
        const dayBeforeEnd = new Date (datetime[1])
        const end = new Date( dayBeforeEnd.getTime() + 86400000)
        match = {...match, datetime: {$gte: start, $lt: end}}
    }

    return match
}

export const createFind = (location, sensorType, datetime, granularity) => {
    let filters = {}

    if (typeof location === 'string'){
       filters = {...filters, location: location}
    }

    if (Array.isArray(location)){
        filters = {...filters, location: {$in: location}}
    }

    if (typeof sensorType === 'string'){
      filters = {...filters, sensorType: sensorType}
    }

    if (Array.isArray(sensorType)){
        filters = {...filters, sensorType: {$in: sensorType}}
    }

    if (typeof datetime === 'string') {

        const time = new Date(datetime)
        const year = time.getFullYear()
        const month = time.getMonth()
        let lt, gte

        switch (granularity) {
            case 'year':
                gte = new Date(year, 0, 1)
                lt = new Date(year + 1, 0, 1)
                break;
            case 'month':
                gte = new Date(year, month, 1)
                lt = new Date(year, month + 1, 1)
                break;
            default:
                gte = time
                lt = new Date(time.getTime() + 86400000)
                break;
        }
        filters = {...filters, datetime: { $gte: gte, $lt: lt}}
    }

    if(Array.isArray(datetime)){
        const gte = new Date(datetime[0])
        const lt = new Date(new Date(datetime[1]).getTime() + 86400000)
        filters = {...filters, datetime: {$gte: gte, $lt: lt}}
    }

    return filters
}