import csv from 'csvtojson'
import { FarmDataModel } from "../models/FarmData.js"
import { FarmLocationsModel } from '../models/FarmLocations.js'
import { validateDataRow, createMatch, createFind, validateLocationData } from "../util/functions.js"

// @desc    Get names and locations of current farms
// @route   GET /api/v1/locations
// @access  public
export const getLocations = async(req, res, next) => {
  const query = FarmLocationsModel.find().select('name coordinates').sort({name:1})
  query.exec((err, show) => {
    if (err) {
      console.log(err)
    }
    if (!err){
      res.status(200).json({locations: show, success: true, msg: 'List of unique farms in database, with locations'})
    }
  })
}

// @desc    Add new Farm name and it's coordinates
// @route   GET /api/v1/locations
// @access  public
export const getNewLocations = async(req, res, next) => {
  const {x, y, z, name} = req.query
  const validation = validateLocationData(x, y, z, name)
  if (validation){
    FarmLocationsModel.create([{name: name, coordinates: {x, y, z}}])
  }
  res.status(200).json({ success: true, msg: "Post new farm data"})
}

// @desc    Get farm names
// @route   GET /api/v1/farms
// @access  public
export const getFarmData = async(req, res, next) => {
  const query = FarmDataModel.distinct("location", (err, data) => {
    res.status(200).json({ success: true, msg: "Get farm names", names: data})
  })
}

// @desc    Post new farm data
// @route   POST /api/v1/farms
// @access  public
export const postFarmData = async(req, res, next) => {
  try{
    for (const file of req.files) {
        const json = await csv().fromString(file.buffer.toString('utf-8'))
        for(const entry of json) {
            const validation = validateDataRow(entry)
            if(validation){
               await FarmDataModel.create(entry)
            }
        };
    }
    res.status(200).json({ success: true, msg: "Post new farm data"})
  }
  catch{
    res.status(500).json({ success: false, msg: "Make sure you are uploading correct csv file"})
  }
}

// @desc    Get metric data
// @route   GET /api/v1/farms/metric
// @access  public
export const getByMetric = async(req, res, next) => {
    const {location, sensorType, datetime, granularity} = req.query
    const find = createFind(location, sensorType, datetime, granularity)
    const query = FarmDataModel.aggregate([
      {
        $match: find
      },
      {
        $group: {
            _id: {location: "$location", day:{$dayOfMonth: "$datetime"}, month: {$month: '$datetime'}, year: {$year: '$datetime'}, hour:{$hour: "$datetime"}, minute:{$minute: "$datetime"}, sensorType:'$sensorType', value: '$value'},
            date: {$first: '$datetime'},
        }
      },
      {
        $project: {
          _id: 1,
          date: 1,
        }
      },
      {
        $group: {
          _id: { location: "$_id.location", date: {$concat : [{ $toString: "$_id.day"},".",{ $toString: "$_id.month"},".",{ $toString: "$_id.year"},"(",{ $toString: "$_id.hour"},".",{ $toString: "$_id.minute"},")"] }},
          info: {
            $push: {
              metric: "$_id.sensorType",
              value: "$_id.value",
              datetime: "$date"
            }
          },
        }
      },
      {
        $sort: {
            "info.datetime": 1,
            "_id.location": 1
        }
      }
    ])


    await query.exec((err, data) => {
        if(err){
            // Handle error
        }
        res.status(200).json({ success: true, msg: "Get farm data", data: data})
    })
}

// @desc    Get statistics (minmax / monthly avg)
// @route   GET /api/v1/farms/stats
// @access  public
export const getStats = async(req, res, next) => {
    const {location, sensorType, datetime, valueInfo, valueGt, valueLt, value, statistics} = req.query
    const match = createMatch(location, sensorType, datetime, valueInfo, valueGt, valueLt, value)
    const queryMonthlyAvg = FarmDataModel.aggregate([
        {
            $match: match
        },
        {
            $group: {
                _id: {location: "$location", month: {$month: '$datetime'}, year: {$year: '$datetime'}, sensorType:'$sensorType'},
                date: {$first: '$datetime'},
                average: {$avg:'$value'}
            }
        },
        {
          $project: {
            _id: 1,
            date: 1,
            average: {$round: ["$average", 2]}
          }
        },
        {
          $group: {
            _id: { location: "$_id.location", date: {$concat : [{ $toString: "$_id.month"},".",{ $toString: "$_id.year"}] }},
            info: {
              $push: {
                metric: "$_id.sensorType",
                average: "$average",
                datetime: "$date"
              }
            },
          }
        },
        {
          $project: {
            _id: 1,
            info: 1,
            datetime: "$info.datetime"
          }
        },
        {
          $sort: {
              datetime: 1
          }
        },
        {
          $project: {
            _id: 1,
            info: 1
          }
        }
    ])

    const queryMinMax = FarmDataModel.aggregate([
        {
            $match: match
        },
        {
          $sort: {
            value: 1
          }
        },
        {
          $group: {
            _id: {
              sensorType: "$sensorType", location: "$location"
            },
            min: {
              $first: "$$ROOT"
            },
            max: {
              $last: "$$ROOT"
            }
          }
        },
        {
          $sort: {
            "_id.location": 1,
            "_id.sensorType": 1,
          }
        },
        {
          $project: {
            _id: 1,
            max: {
              value: "$max.value",
              month: {$month: '$max.datetime'}, year: {$year: '$max.datetime'}, day: {$dayOfMonth: '$max.datetime'}
            },
            min: {
              value: "$min.value",
              month: {$month: '$min.datetime'}, year: {$year: '$min.datetime'}, day: {$dayOfMonth: '$min.datetime'}
            }
          }
        },
        {
          $group: {
            _id: "$_id.location",
            sensorData: {
              $push: {
                k: "$_id.sensorType",
                v: {
                  max: "$max",
                  min: "$min"
                }
              }
            }
          }
        },
        {
          "$replaceRoot": {
            "newRoot": {
              "$arrayToObject": [
                [
                  {
                    k: "$_id",
                    v: {
                      "$arrayToObject": "$sensorData"
                    }
                  }
                ]
              ]
            }
          }
        }
    ])

    let query
    switch(statistics){
        case 'minmax':
            query = queryMinMax
            break;
        case 'average':
            query = queryMonthlyAvg
            break;
        default:
            break;
    }

   await query.exec((err, data) => {
        if(err){
            console.log("Error: ")
            console.log(err)
        }
        console.log("Show: ")
        res.status(200).json({ success: true, msg: "Get farm data", data: data})
    })
}
