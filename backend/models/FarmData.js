import mongoose from 'mongoose'

const FarmDataModelSchema = new mongoose.Schema({
    location:{
        type: String,
    },
    datetime: {
        type: Date,
        },
    sensorType: {
        type: String,
    },
    value:{
        type: Number,
    }
})

export const FarmDataModel = mongoose.model('FarmDataModel', FarmDataModelSchema)
