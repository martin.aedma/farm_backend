import mongoose from 'mongoose'

const FarmLocationsModelSchema = new mongoose.Schema({
    name:{
        type: String,
    },
    coordinates:{
        type: Object,
    }
})

export const FarmLocationsModel = mongoose.model('FarmLocationsModel', FarmLocationsModelSchema)
