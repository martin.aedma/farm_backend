import express from "express";
import { postFarmData, getByMetric, getStats, getLocations, getNewLocations, getFarmData } from "../controllers/farms.js";

const farms = express.Router()

// Routes
farms.route('/').post(postFarmData).get(getFarmData)
farms.route('/metric').get(getByMetric)
farms.route('/stats').get(getStats)
farms.route('/locations').get(getLocations)
farms.route('/newlocations').get(getNewLocations)


export default farms