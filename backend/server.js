import express from 'express'
import dotenv from 'dotenv'
import morgan from 'morgan'
import multer from 'multer'
import farms from './routes/farms.js'
import {connectDB} from './config/db.js'
import colors from 'colors'
import cors from 'cors'

// Load env files
dotenv.config({ path: './config/config.env'})

const app = express()
const PORT = process.env.PORT || 5000
connectDB()

app.use(cors({
    origin: 'http://localhost:3000'
}))

// Middlewares
    // Dev logging
if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'))
}
    // Enable file upload
app.use(multer({ storage: multer.memoryStorage() }).array('datafiles', 4))

// Routes
app.use('/api/v1/farms', farms)

// Listen server
app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} on port ${PORT}`.yellow.bold))

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
    console.log(`Error: ${err.message}`.red)
    // Close server & exit process
    process.exit(1)
})