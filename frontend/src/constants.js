export const API_ROOT = 'http://localhost:5000/api/v1/farms'
export const API_URL_LOCATIONS = API_ROOT + '/locations'
export const API_URL_METRIC = API_ROOT + '/metric'
export const API_URL_STATS = API_ROOT + '/stats'
export const API_URL_NEW_LOCATIONS = API_ROOT + '/newlocations'


