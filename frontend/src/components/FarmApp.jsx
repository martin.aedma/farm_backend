import React, { useEffect, useState } from "react";
import { Menu } from "./Menu";
import { Query } from "./Query";
import { Results } from "./Results";
import { Background } from "./Background";
import { AddFile } from "./AddFile";
import { API_ROOT, API_URL_LOCATIONS, API_URL_NEW_LOCATIONS } from "../constants";
import dynamic from 'next/dynamic'

const DynamicScene = dynamic(() => import('./Map'), {ssr: false})

export const FarmApp = () => {

    const [resultViewOptions, setResultViewOptions] = useState({isViewingGraph: true, currentDataSet: 1, maxSets: 0})
    const [viewState, setViewState] = useState({current: 'home', next: false})
    const [viewSwitch, setViewSwitch] = useState(false)
    const [farmLocations, setFarmLocations] = useState(false)
    const [queryResults, setQueryResults] = useState({data: false, previousQuery: false})
    const [isMapLoaded, setIsMapLoaded] = useState(false)

    useEffect(() => {
        if (viewState.next) {setViewSwitch(true)}
        !farmLocations ? getLocations() : farmLocations.needsUpdate ? getLocations() : {}
    }, [viewState, farmLocations])

    const mapText = <>
        <div className="map-name">
            <h1>iFarm</h1>
            <span>Greatest Farm data search App on </span>
            <span> in the making</span>
        </div>
        <div className="map-instructions">
            <span>click and drag to </span>
        </div>
    </>

    const getLocations = async() => {
        // Get all current locations
        const getLocations = await fetch(API_URL_LOCATIONS, {
            method: 'GET',
            headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json',
            }
        })
        const locations = await getLocations.json()

        const locationsArray = locations.locations.map((item, i) => {
            return item.name
        })

        // Get all unique farm names in database
        const getNames = await fetch(API_ROOT, {
            method: 'GET',
            headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json',
            }
        })
        const names = await getNames.json()

        // Check for new Locations
        const newLocations = names.names.filter(name => !locationsArray.includes(name))

        // If there are any new locations, add them to database
        if(newLocations.length > 0) {
            for (const loc of newLocations){
                const params = [["name", loc], ["x","1"], ["y","2"], ["z", "3"]]
                const searchParams = new URLSearchParams(params).toString()
                const postLocations = await fetch(API_URL_NEW_LOCATIONS+'?'+searchParams, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }
                })
                const postResult = await postLocations.json()
            }
        }
        setFarmLocations({...locations, sensorTypes: ['temperature', 'pH', 'rainFall'], needsUpdate : newLocations.length > 0 ? true : false})
    }

    const displayContent = () => {
        switch(viewState.current){
            case 'home':
                return <>
                    {isMapLoaded && mapText}
                    <Background/>
                    <DynamicScene farmLocations={farmLocations} setIsMapLoaded={setIsMapLoaded}/>
                </>
            case 'query':
                return <Query farmLocations={farmLocations} setViewState={setViewState} queryResults={queryResults} setQueryResults={setQueryResults}/>
            case 'results':
                return <Results queryResults={queryResults} resultViewOptions={resultViewOptions} setResultViewOptions={setResultViewOptions}/>
            case 'addfile':
                return <AddFile/>
            default:
                return <DynamicScene/>
        }
    }

    const handleAnimationEnd = () => {
        if(viewSwitch){
            setViewState(prev => {return {current: prev.next, next: false}})
            setViewSwitch(false)
        }
    }

return <>
    <div className="farmapp-container">
        <div className="appversion-container">
            <span>Alpha v1.00</span>
        </div>
        <div className="content-background"></div>
        <div className={ viewSwitch ? "content-container fade-out" : 'content-container fade-in'} onAnimationEnd={handleAnimationEnd}>
            {displayContent()}
        </div>
        <div className="footer-container">
            <a href="https://www.linkedin.com/in/martin-aedma-7380aa1a7/" target="_blank" rel="noopen noreferrer">
                <img src="/linkedin.svg"/>
            </a>
        </div>
        <Menu viewState={viewState} setViewState={setViewState} resultViewOptions={resultViewOptions} setResultViewOptions={setResultViewOptions} queryResults={queryResults}/>
    </div>
</>
}
