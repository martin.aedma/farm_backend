import React, { useState } from "react";
import { API_ROOT } from "../constants";
import axios from 'axios'

export const AddFile = () => {

    const [uploadFile, setUploadFile] = useState([])
    const [isUpLoading, setIsUploading] = useState(false)

    const handleSubmit = async(e) => {
        e.preventDefault()
        if (!uploadFile[0]){
            return
        }

        setIsUploading(true)
        const dataArray = new FormData();
        for(const file of uploadFile){
            dataArray.append("datafiles", file);
        }
        let success = false

        try {
        const response = await axios.post( API_ROOT,
            dataArray, {
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'multipart/form-data'
                  }
        })
        const result = await response
        success = true
        } catch (error) {
            setIsUploading(false)
            setUploadFile([])
            success = false
            console.log(error.response)
        }
        success ? window.location.reload() : {}
    }

    return <div className="addfile-container">
        {!isUpLoading && <div style={{textAlign: 'center'}}>
            <label htmlFor="files">Select file(s) {`[${uploadFile.length}]`}</label>
            <input type="file" id="files" name="files" multiple onChange={(e) => setUploadFile(e.target.files)}/>
            <div className="btn" onClick={handleSubmit}><span>upload</span></div>
        </div>}
        {isUpLoading && <div className="uploading-container">
            <span>UPLOADING</span>
            <div className='loader'>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>}
    </div>
}