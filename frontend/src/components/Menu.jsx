import React, { useEffect, useState } from "react";

export const Menu = ({viewState, setViewState, resultViewOptions, setResultViewOptions, queryResults}) => {

    const [toggleChartIcon, setToggleChartIcon] = useState(false)

    useEffect(() => {
        if(resultViewOptions.isViewingGraph){
            if ( queryResults.data && resultViewOptions.maxSets > 1){
                setToggleChartIcon(true)
            }
        } else {
            if (toggleChartIcon) {setToggleChartIcon (false)}
        }
    }, [resultViewOptions, queryResults])

    const handleClick = (e, indicator) => {
        switch(indicator){
            case 'home':
                if (viewState.current !== 'home') {setViewState(prev => {return {...prev, next: 'home'}})}
                break;
            case 'query':
                if (viewState.current !== 'query') {setViewState(prev => {return {...prev, next: 'query'}})}
                break;
            case 'addfile':
                if(viewState.current !== 'addfile') {setViewState(prev => {return {...prev, next: 'addfile'}})}
                break;
            case 'table':
                if (viewState.current === 'results') {
                    setResultViewOptions(prev => { return {...prev, isViewingGraph: false}})
                } else if (queryResults.data && queryResults.data.length !== 0) {
                    setViewState(prev => {return {...prev, next: 'results'}})
                    setResultViewOptions(prev => { return {...prev, isViewingGraph: false}})
                }
                break;
            case 'chart':
                if (viewState.current === 'results') {
                    setResultViewOptions(prev => { return {...prev, isViewingGraph: true}})
                } else if (queryResults.data && queryResults.data.length !== 0) {
                    setViewState(prev => {return {...prev, next: 'results'}})
                    setResultViewOptions(prev => { return {...prev, isViewingGraph: true}})
                }
                break;
            case 'nextset':
                if (viewState.current === 'results') {
                    setResultViewOptions(prev => {return {...prev, currentDataSet: prev.currentDataSet + 1 > prev.maxSets ? 1 : prev.currentDataSet + 1 }})
                } else if (queryResults.data && queryResults.data.length !== 0) {
                    setViewState(prev => {return {...prev, next: 'results'}})
                    setResultViewOptions(prev => {return {...prev, currentDataSet: prev.currentDataSet + 1 > prev.maxSets ? 1 : prev.currentDataSet + 1 }})
                }
                break;
            default:
                break;
        }
    }

    return <>
        <div className="menu-container">
            <div className="menu">
                    <img src='/globe.svg' onClick={(e) => handleClick(e, "home")}/>
                    <img src='/table.svg' onClick={(e) => handleClick(e, "table")}/>
                    <img src={toggleChartIcon ? '/next.svg' : 'chart.svg'} onClick={(e) => handleClick(e, toggleChartIcon ? 'nextset' : 'chart')}/>
                    <img src="/search.svg" onClick={(e) => handleClick(e, "query")} />
                    <img src="/file.svg" onClick={(e) => handleClick(e, "addfile")} />
            </div>
        </div>
    </>
}