import React, { useEffect, useState } from "react";
import { API_URL_METRIC, API_URL_STATS } from "../constants";
import { Selection } from "./Selection";

export const Query = ({farmLocations, setViewState, setQueryResults, queryResults}) => {

    const [queryBuilder, setQueryBuilder] = useState({location: [], datetime: [], sensorType: [], granularity: [], statistics: []})

    useEffect(() => {
        queryResults.previousQuery ? setQueryBuilder(queryResults.previousQuery) : {}
    }, [])

    const isQueryBuilderEmpty = () => {
        let i = 0
        for (const option of Object.keys(queryBuilder)) {
            queryBuilder[option].length > 0 ? i++ : ''
        }
        return i === 0 ? true : false
    }

    const clearAllOptions = () => {
        setQueryBuilder({location: [], datetime: [], sensorType: [], granularity: [], statistics: []})
    }

    const createCurrentDate = () => {
        const date = new Date()
        return `${date.getUTCFullYear()}-${date.getUTCMonth()+1}-${date.getUTCDate()}`
    }

    const validateDateTime = () => {
        if (queryBuilder.datetime.length === 0){
            return [createCurrentDate()]
        }
        const keys = Object.keys(queryBuilder.datetime)
        switch (keys.length){
            case 1:
                return queryBuilder.datetime[keys[0]] === '' ? [createCurrentDate()] : [queryBuilder.datetime[keys[0]]]
            default:
                const dates = {first: queryBuilder.datetime[keys[0]], second: queryBuilder.datetime[keys[1]]}
                if (dates.first === '' && dates.second === ''){
                    return [createCurrentDate()]
                }
                if (dates.first === ''){
                    return [dates.second]
                }
                if (dates.second === '') {
                    return [dates.first]
                }
                return new Date(dates.first) < new Date(dates.second) ? [dates.first, dates.second] : [dates.second, dates.first]
        }
    }

    const handleExecute = async() => {
        const options = {...queryBuilder, datetime: validateDateTime()}
        const queryParamKeys = Object.keys(options)
        const queryParams = []
        for (const paramKey of queryParamKeys) {
            if(options[paramKey].length > 0){
                for(const option of options[paramKey]){
                    queryParams.push([paramKey, option])
                }
            } else {
                switch(paramKey){
                    case 'location':
                        for (const location of farmLocations.locations){
                            queryParams.push([paramKey, location.name])
                        }
                        break;
                    case 'sensorType':
                        for (const type of farmLocations.sensorTypes){
                            queryParams.push([paramKey, type])
                        }
                        break;
                    case 'granularity':
                        queryParams.push([paramKey, 'day'])
                        break;
                    case 'type':
                        queryParams.push([paramKey, 'find'])
                        break;
                }
            }
        }

        const URL = queryBuilder.statistics.length === 0 ? API_URL_METRIC : API_URL_STATS
        const searchParams = new URLSearchParams(queryParams).toString()
        const response = await fetch(URL + '?' + searchParams, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
        }})
        const result = await response.json()
        setQueryResults({data: result.data, previousQuery: {...options, datetime: {from: options.datetime[0], to: options.datetime[1] ? options.datetime[1] : options.datetime[0]}, type: options.statistics[0] ? options.statistics[0] : 'find'}})
        setViewState(prev => {return {...prev, next: 'results'}})
    }

    return <>
        <div className="grid-options-container">
            <div className="query-header">
                <h1>create query
                    <div className="query-options-container">
                        <p className="query-options"> options</p>
                        {!isQueryBuilderEmpty() && <span className="query-options" style={{cursor: 'pointer'}} onClick={clearAllOptions}>[clear]</span>}
                    </div>
                </h1>
            </div>

            <div className='options-container fade-in'>
                <Selection farmLocations={farmLocations} queryBuilder={queryBuilder} setQueryBuilder={setQueryBuilder}/>
            </div>

            <div className="execute-container">
                <div className="btn" onClick={handleExecute}>execute</div>
            </div>
        </div>
    </>
}

