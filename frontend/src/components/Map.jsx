import React, { Suspense, useEffect } from "react";
import { Canvas } from '@react-three/fiber'
import { PerspectiveCamera, OrbitControls, Loader } from '@react-three/drei'
import Earth from './Earth'

const Map = ({farmLocations, setIsMapLoaded}) => {

    useEffect(() => {
        setIsMapLoaded(true)
    }, [])

    return <div className="webgl-canvas">
    <Canvas>
        <ambientLight intensity={0.75}/>
        <PerspectiveCamera makeDefault position={[0, -10, 8]} />
        <OrbitControls enablePan={false} enableZoom={false}/>
        <Suspense fallback={null} r3f>
            <Earth farmLocations={farmLocations}/>
        </Suspense>
    </Canvas>
    <Loader />
</div>
}

export default Map