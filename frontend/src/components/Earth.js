import React, { useEffect, useRef, useState } from 'react'
import { useFrame } from '@react-three/fiber'
import { Box, useGLTF, MeshWobbleMaterial, Html } from '@react-three/drei'

export default function Model({ farmLocations }) {
  const refs = [useRef(), useRef(), useRef(), useRef()]

  const coordinates = [
    {position: [0, 0.7, 0.9], rotation: [0.9, 0, 0], color: 'blue'},
    {position: [1, -0.3, -0.45], rotation: [0.1, 0.4, 1.3], color: 'purple'},
    {position: [-0.55, -0.5, -0.85], rotation: [-0.3, 1.9, 1.6], color: 'red'},
    {position: [0, 0.8, -0.8], rotation: [0.9, 1.6, 1.6], color: 'brown'}
  ]

  const [hovered, setHovered] = useState({index: false})

  useFrame(({clock}) => {
    group.current.rotation.y = clock.getElapsedTime() * 0.02
    group.current.rotation.z = clock.getElapsedTime() * 0.02
    group.current.rotation.x = clock.getElapsedTime() * 0.02
    if (hovered.index !== false && hovered.index < 4) {
      refs[hovered.index].current.rotation.z = clock.getElapsedTime()
      refs[hovered.index].current.rotation.y = clock.getElapsedTime()
    }
  })

  useEffect(() => {
    document.body.style.cursor = hovered ? 'pointer' : 'auto'
    return () => {document.body.style.cursor = 'auto'}
  }, [hovered])

  const renderLocations = () => {
    if(farmLocations){
      return farmLocations.locations.map((location, i) => {
        return <Box
          ref={refs[i]}
          key={i}
          position={i < 4 ? coordinates[i].position : [0, 0, 0]}
          rotation={i < 4 ? coordinates[i].rotation : [0, 0, 0]}
          onPointerEnter={() => setHovered({index: i})}
          scale={0.1}>
            {hovered.index === i && <Html className='location-box-header'>
              <h2 style={{color: i < 4 ? coordinates[i].color : 'green', width: '300px' }}>{location.name}</h2>
              <h4 style={{color: i < 4 ? coordinates[i].color : 'green', width: '300px' }}>{`Location : ${i < 4 ? coordinates[i].position : [0, 0, 0]}`}</h4>
            </Html>}
            <MeshWobbleMaterial factor={0.5} speed={0.5} attach="material" wireframe color={i < 4 ? coordinates[i].color : 'green'} />
        </Box>
      })
    }
  }

  const group = useRef()
  const { nodes, materials } = useGLTF('/earth.glb')
  return (
    <group ref={group} dispose={null} scale={4}>
      <group rotation={[-3.14, 0.87, 3.14]}>
        {renderLocations()}
        <mesh geometry={nodes.Sphere002.geometry} material={materials['Material.009']} />
        <mesh geometry={nodes.Sphere002_1.geometry} material={materials['Material.008']} />
        <mesh geometry={nodes.Sphere002_2.geometry} material={materials['Material.010']} />
        <mesh geometry={nodes.Sphere002_3.geometry} material={materials['Material.005']} />
      </group>
    </group>
  )
}

useGLTF.preload('/earth.glb')
