import React, {useState} from "react";

export const Selection = ({farmLocations, queryBuilder, setQueryBuilder}) => {

    const availableOptions = {
        sensorType: farmLocations.sensorTypes,
        granularity: ['day', 'month', 'year'],
        statistics: ['average', 'minmax']
    }

     const handleClick = (e, option, category) => {
        if (!queryBuilder[category].includes(option)){
            setQueryBuilder( prev => {return {...prev, [category]: category === 'sensorType' ? [...prev[category], option] : [option] }})
        } else {
            setQueryBuilder(prev => {return {...prev, [category]: category === 'sensorType' ? queryBuilder[category].filter(item => item !== option) : [] }})
        }
    }

    const mapOptionCategory = (category) => {
       const elements = availableOptions[category].map((option, i) => {
            const includes = queryBuilder[category].includes(option)
            return <div key={i} className="options selection-option" onClick={(e) => {handleClick(e, option, category)}}>
                <span id={option} className={ includes ? 'check-box check-box-selected' : "check-box"}></span>
                <img src={`/options_icons/${option}.svg`}/>
                <span className={includes ? 'color-red' : ''}>[{option}]</span>
            </div>
        })
        return elements
    }

    return <>
        <div className="selection-container">
            <Location farmLocations={farmLocations} queryBuilder={queryBuilder} setQueryBuilder={setQueryBuilder}/>
            {mapOptionCategory('sensorType')}
            {mapOptionCategory('granularity')}
            {mapOptionCategory('statistics')}
            <DatePicker queryBuilder={queryBuilder} setQueryBuilder={setQueryBuilder} direction='from' />
            <DatePicker queryBuilder={queryBuilder} setQueryBuilder={setQueryBuilder} direction='to' />
        </div>
    </>
}

const Location = ({farmLocations, queryBuilder, setQueryBuilder}) => {

    const [searchString, setSearchString ] = useState('')
    const [displayAllLocations, setDisplayAllLocations] = useState(false)
    const [firstLoad, setFirstLoad] = useState(false)

    const handleClick = () => {
        setDisplayAllLocations(!displayAllLocations)
        if(!firstLoad){
            setFirstLoad(true)
        }
    }

    const handleSelect = (e, name) => {
        setQueryBuilder(prev => {return {...prev, location: !queryBuilder.location.includes(name) ? [...prev.location, name] : queryBuilder.location.filter(item => item !== name)}})
    }

    const mapLocations = () => {
        const locations = searchString.length === 0 ? farmLocations.locations : farmLocations.locations.filter((item, i) => {return item.name.toLowerCase().includes(searchString)})
        return locations.map((item, i) => { return <span className={queryBuilder.location.includes(item.name) ? 'selected-location' : "location-option"} key={i} onClick={(e) => handleSelect(e, item.name)}>{item.name}</span>})
    }

    return <>
        <div className="location">
                <div className="location-header">
                    <img src="options_icons/location.svg"/>
                    <span className={queryBuilder.location.length > 0 ? 'color-red' : ""}>[location]</span>
                </div>

                <div className="location-header">
                    <input className="location-filter-input" type='text' onChange={(e) => { setSearchString(e.target.value.toLowerCase())}}/>
                    <img src="/arrow.svg"
                        className={firstLoad ? displayAllLocations ? "list-arrow rotate-up" : 'list-arrow rotate-down' : 'list-arrow'}
                        onClick={handleClick}
                    />
                </div>
                <div className={firstLoad ? displayAllLocations ? "location-container slide-down" : 'location-container slide-up' : 'location-container'}>
                    {mapLocations()}
                </div>
            </div>
    </>
}

const DatePicker = ({queryBuilder, setQueryBuilder, direction}) => {
    const handleChange = (e) => {
        setQueryBuilder( prev => { return { ...prev, datetime: {...prev.datetime, [direction]: e.target.value}}})
    }
    return <>
        <div className="selection-option datetime">
            <div className="location-header">
                <img src={`options_icons/${direction}.svg`}/>
                <input
                    type='date'
                    onChange={handleChange}
                    value={queryBuilder.datetime[direction] ? queryBuilder.datetime[direction] : ''}
                />
            </div>
        </div>
    </>
}