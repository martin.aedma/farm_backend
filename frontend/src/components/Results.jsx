import React, { useEffect, useState } from "react";
import { BarChart, Bar, XAxis, YAxis, ReferenceLine, Tooltip, ResponsiveContainer, ComposedChart, Area, Line, Legend, Label } from 'recharts';

export const Results = ({queryResults, resultViewOptions, setResultViewOptions}) => {

    const [data, setData] = useState({tableData: [], graphData: []})
    useEffect(() => {
        setData(transformData(queryResults.previousQuery.type))
    }, [])

    const transformData = (queryType) => {
        let dataObject
        switch (queryType) {
            case 'minmax':
                dataObject = formatMinMaxData(queryResults.data)
                break;
            case 'average':
                dataObject = formatAverageData(queryResults.data)
                break;
            default:
                dataObject = formatGranularityData(queryResults.data)
                break;
        }
        setResultViewOptions( prev => { return {...prev, maxSets: dataObject.graphData.length}})
        return dataObject
    }

    return <>
        {data.graphData.length === 0 ? <NoResults/> : resultViewOptions.isViewingGraph ? <GraphView resultViewOptions={resultViewOptions} data={data.graphData} type={queryResults.previousQuery.type}/> : <TableView data={data.tableData} type={queryResults.previousQuery.type}/> }
    </>
}

const formatMinMaxData = (data) => {
    const dataObject={tableData: [], graphData: []}
    for (const [i, item] of data.entries()){
        const sensors = Object.keys(Object.values(item)[0])
        const location = Object.keys(item)[0]
        const graphObj = {location: location}
        for (const [j, sensor] of sensors.entries()) {
            // graph data
            graphObj = {...graphObj, [sensor + 'Min']: item[location][sensor].min.value, [sensor + 'Max']: item[location][sensor].max.value}
            // table data
            dataObject.tableData.push(<tr key={i * sensors.length + j} className="table-row">
                <td>{location}</td>
                <td><img src={`/options_icons/${sensor}.svg`}/></td>
                <td title={`${item[location][sensor].min.day}.${item[location][sensor].min.month}.${item[location][sensor].min.year}`}>{item[location][sensor].min.value}</td>
                <td title={`${item[location][sensor].max.day}.${item[location][sensor].max.month}.${item[location][sensor].max.year}`}>{item[location][sensor].max.value}</td>
            </tr>)
        }
        dataObject.graphData.push(graphObj)
    }
    return dataObject
}

const formatAverageData = (data) => {
    const dataObject={tableData: [], graphData: []}
    for (const [i, item] of data.entries()){
        // for graph data formatting create object per unique location
        let count = 0
        for(const graphData of dataObject.graphData){
            graphData.location === item._id.location ? count++ : {}
        }
        // unique location found, add it
        count === 0 ? dataObject.graphData.push({location: item._id.location, data: []}) : {}
        const metricDataObj = {date: item._id.date}

        for (const [j, entry] of item.info.entries()){
            // Create table data
            dataObject.tableData.push(
                <tr key={j *data.length + i} className="table-row">
                <td>{item._id.location}</td>
                <td><img src={`/options_icons/${entry.metric}.svg`}/></td>
                <td>{item._id.date}</td>
                <td>{entry.average}</td>
                </tr>)
            // Create Graph data object
            metricDataObj = {...metricDataObj, [entry.metric]: entry.average}
        }

        for(const graphData of dataObject.graphData){
            if(graphData.location === item._id.location){
                graphData.data.push(metricDataObj)
            }
        }
    }
    return dataObject
}

const formatGranularityData = (data) => {
    const dataObject={tableData: [], graphData: []}
    for (const [i, item] of data.entries()){
        let count = 0
        for(const graphData of dataObject.graphData){
            graphData.location === item._id.location ? count++ : {}
        }
        // unique location found, add it
        count === 0 ? dataObject.graphData.push({location: item._id.location, data: []}) : {}
        const metricDataObj = {date: item._id.date}
        for (const [j, entry] of item.info.entries()){
            metricDataObj = {...metricDataObj, [entry.metric]: entry.value}
            dataObject.tableData.push(
                <tr key={j * data.length + i} className="table-row">
                <td>{item._id.location}</td>
                <td><img src={`/options_icons/${entry.metric}.svg`}/></td>
                <td>{item._id.date}</td>
                <td>{entry.value}</td>
                </tr>)
        }
        for(const graphData of dataObject.graphData){
            if(graphData.location === item._id.location){
                graphData.data.push(metricDataObj)
            }
        }
    }
    return dataObject
}

const TableView = ({data, type}) => {

    const tableHeaders = {
        minmax : <tr>
            <th>Farm</th>
            <th>Metric</th>
            <th>Min</th>
            <th>Max</th>
        </tr>,
        average : <tr>
            <th>Farm</th>
            <th>Metric</th>
            <th>Month</th>
            <th>Average</th>
        </tr>,
        find: <tr>
            <th>Farm</th>
            <th>Metric</th>
            <th>Value</th>
            <th>Date</th>
        </tr>,
    }

    return <>
        <table>
            <thead>
                {tableHeaders[type]}
            </thead>
            <tbody>
                {data}
            </tbody>
        </table>
    </>
}

const GraphView = ({data, type, resultViewOptions}) => {

    const bars = []
    for (const item of data){
        const keyes = Object.keys(item)
        keyes.shift()
        for (const key of keyes) {
            !bars.includes(key) ? bars.push(key) : {}
        }
    }

    const renderCustomBarLabel = ({ payload, x, y, width, height, value }) => {
        return <text x={x + width / 2} y={y} fill="#3c3a5a" textAnchor="middle" dy={-6} fontSize={'1.5rem'}>{value}</text>;
      };


    const renderLineChart = (
        <div className="chart-container">
            <ResponsiveContainer width='100%' height='100%'>
                <BarChart data={[data[resultViewOptions.currentDataSet-1]]} margin={{top: 20}}>
                <Tooltip cursor={{fill:'transparent'}} position={{x: 0, y: 0}}/>
                <XAxis dataKey="location" fontSize={'1.5rem'} stroke="#3c3a5a">
                    <Label value={`${resultViewOptions.currentDataSet}/${resultViewOptions.maxSets}`} offset={10} position="insideTopRight"/>
                </XAxis>
                <YAxis stroke="#3c3a5a" fontSize={'1.5rem'}/>
                <ReferenceLine y={0} stroke="#f10c0c"/>
                {bars.map((bar, i) => {
                    return <Bar key={i} dataKey={bar} fill={bar.includes('Min') ? '#B6CAEF' : '#3c3a5a'} isAnimationActive={false} label={renderCustomBarLabel}/>
                })}
                </BarChart>
            </ResponsiveContainer>
        </div>
    );


    const renderAverageChart = (
        <div className="chart-container">
        <ResponsiveContainer width='100%' height='100%'>
            <ComposedChart data={data[resultViewOptions.currentDataSet-1].data} margin={{top: 20, bottom: 20}}>
            <YAxis stroke="#3c3a5a" fontSize={'1.5rem'}/>
            <ReferenceLine y={0} stroke="#f10c0c"/>
            <Legend/>
            <Tooltip/>
            <Bar dataKey='pH' stroke="#3c3a5a" fill="#3c3a5a" isAnimationActive={false}/>
            <Line type="monotone" dataKey="temperature" fill="#ff7300" stroke="#ff7300" isAnimationActive={false}/>
            <Area type="monotone" dataKey="rainFall" fill="#8884d8" stroke="#8884d8" isAnimationActive={false}/>
            <XAxis textAnchor="middle" fontSize={'1rem'} dataKey='date' stroke="#3c3a5a">
                <Label fill="#f10c0c" stroke="#f10c0c"  value={`${resultViewOptions.currentDataSet}/${resultViewOptions.maxSets} ` + data[resultViewOptions.currentDataSet-1].location} offset={-30} position="insideTopRight"/>
            </XAxis>
            </ComposedChart>
        </ResponsiveContainer>
    </div>
    )

    const displayChart = () =>{

        if(data.length === 0){
            return
        }

        switch (type) {
            case 'minmax':
                return renderLineChart
            default:
                return renderAverageChart
        }
    }

    return <>
        {displayChart()}
    </>
}

const NoResults = () => {
    return <>
        <div className="no-results-container">
           <h1>No data found</h1>
        </div>
    </>
}